package test.expection;

/**
 * 编译错误
 * 
 * @author jinmiao 2014-8-4 上午9:44:19
 */
public class CompilerExpection extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CompilerExpection(String message) {
		super(message);
	}
}
