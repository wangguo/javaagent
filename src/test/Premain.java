package test;

import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;

import test.thread.CheckThread;

public class Premain {
	public static void premain(String agentArgs, Instrumentation inst)
			throws ClassNotFoundException, UnmodifiableClassException {
		System.out.println("agent参数"+agentArgs);
		CheckThread thread  = new CheckThread(inst);
		thread.start();
	}
}
