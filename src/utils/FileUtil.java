package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件工具
 * 
 * @author jinmiao 2014-8-4 上午9:57:17
 */
public class FileUtil {
	/**
	 * 加载文件成二进制
	 * 
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public static byte[] loadClassData(String name) throws IOException {
		// 读取类文件
		File file = new File(name);
		if (!file.exists()) {
			return null;
		}
		FileInputStream input = null;
		try {
			input = new FileInputStream(file);
			long length = file.length();
			byte[] bt = new byte[(int) length];
			int rl = input.read(bt);
			if (rl != length) {
				throw new IOException("不能读取所有内容");
			}
			return bt;
		} 
		finally
		{
			if(input!=null)
				input.close();
		}
	}
	
	/**
	 * 获得文件上次修改时间
	 * @param name
	 * @return
	 */
	public static long getFileLastModifyTime(String name)
	{
		// 读取类文件
		File file = new File(name);
		return file.lastModified();
	}

	
	/**
	 * 设置文件修改日期
	 * @param name
	 */
	public static void writeFileLastModifyTime(String name,long time)
	{
		// 读取类文件
		File file = new File(name);
		file.setLastModified(time);
	}
	
	
	
	
	public static void main(String[] args) throws IOException {
	}
	
	
	
	/**
	 * 根据路径获得该java类的包名
	 * @param classPath
	 * @return
	 * @throws IOException
	 */
	public static String getClassPackage(String classPath) throws IOException
	{
		InputStream in = null; 
		BufferedReader reader = null;
		try
		{
			in = new FileInputStream(classPath);;
			reader = new BufferedReader(new InputStreamReader(in)) ;
			String packageName ="";
			//截取包名
			String line="";
			while((line=reader.readLine())!=null)
			{
				if(line.indexOf("package")!=-1)
				{
					//package logic.scene; 变成 logic.scene
					packageName = line.substring(line.indexOf(" "),line.length()-1).trim();
					break;
				}
			}
			return packageName;
		}
		finally
		{
			if(reader!=null)
				reader.close();
			if(in!=null)
				in.close();
		}
	}
	
	
	/**
	 * 以UTF-8读取文件内容
	 * @param fileName
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static String readStringFileUTF8(String fileName) throws Exception
	{
		return readStringFile(fileName, "UTF-8");
	}
	
	/**
	 * 读取文件内容
	 * 
	 * @param fileName
	 * @param encoding
	 * @return file content
	 */
	public static String readStringFile(String fileName, String encoding)
			throws Exception
	{
		FileInputStream fis = null;
		BufferedReader reader =null;
		try
		{
			fis = new FileInputStream(fileName);
			reader = new BufferedReader(new InputStreamReader(fis,
					encoding));
			StringBuffer sb = new StringBuffer();
			while (reader.ready())
			{
				String line = reader.readLine();
				sb.append(line);
				sb.append("\r\n");
			}
			return sb.toString();
		}
		finally
		{
			if(reader!=null)
				reader.close();
			if(fis!=null)
				fis.close();
		}
	}
	
	/**
	 * 删除一个文件或者目录
	 * 
	 * @param file
	 */
	public static void delete(File file) {
		if (file.isFile()) {
			file.delete();
		} else if (file.isDirectory()) {
			File[] _files = file.listFiles();
			for (File _f : _files) {
				delete(_f);
			}
			file.delete();
		}
	}
	/**
	 * 获得目录下面所有文件
	 * @param root
	 * @return
	 */
	public static List<String> listDirAllFiles(String root) {
		File dir = new File(root);
		root = dir.getAbsolutePath();

		List<String> fileNames = new ArrayList<String>();

		File[] files = dir.listFiles();
		for (File file : files) {
			listAllFiles(root, file, fileNames);
		}

		return fileNames;
	}
	
	private static void listAllFiles(String root, File dir,
			List<String> fileNames) {
		if (dir.isDirectory()) {
			java.io.File[] children = dir.listFiles();
			for (File file : children) {
				listAllFiles(root, file, fileNames);
			}
		} else {
			String path = dir.getAbsolutePath();
			String fileName = path.substring(root.length() + 1);
			fileNames.add(fileName);
		}
	}
	
}
